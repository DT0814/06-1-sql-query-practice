/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 *
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
select od.orderNumber, sum(ord.priceEach * ord.quantityOrdered) totalPrice
from orders od,
     orderdetails ord
where od.orderNumber = ord.orderNumber
group by od.orderNumber
having sum(ord.priceEach * ord.quantityOrdered) > 60000
