/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
select concat(em1.lastName,', ',em1.firstName) employee ,concat(em2.lastName,', ',em2.firstName)  manager
    from employees em1, employees em2
    where em1.reportsTo is not null and em1.reportsTo = em2.employeeNumber
        order by manager,employee
